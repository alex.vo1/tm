// ==UserScript==
// @name         CDH Vendor Documentation Button
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Inserts 'Gotcha' documentation when adding a new connector
// @match        https://sso.tealiumiq.com/tms*?*account=*
// @grant        GM.xmlHttpRequest
// ==/UserScript==

(function() {
    'use strict';

    // Lookup table for vendor documentation stored in Google Sheets
    const spreadsheetUrl = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT4vpgFpXOzVyY1P4OQjRRn3rYJ0grWE0Sy0PzH7EQLC3NXQe-M-te5T3mEZggjsOfEQtRQKYyZ7QJm/pub?gid=0&single=true&output=csv'
    var lookupTable = {};

    // Function to fetch the lookup table data from the spreadsheet
    function fetchLookupTable(callback, displayName) {
        // Check if the lookup table data is already in local storage
        var storedLookupTable = localStorage.getItem('CDHlookupTable');
        var storedTimestamp = localStorage.getItem('CDHlookupTableTimestamp');
        var currentTimestamp = Date.now();
        if (storedLookupTable && storedTimestamp) {
            lookupTable = JSON.parse(storedLookupTable);
            var timeSinceLastUpdate = currentTimestamp - parseInt(storedTimestamp);
            var lookupTableRefreshTime = 7 * 24 * 60 * 60 * 1000; // 7 days in milliseconds. This can be updated to a longer cadence.
            if (timeSinceLastUpdate >= lookupTableRefreshTime) {
                // If it has been more than 7 days, fetch a new copy of the lookup table
                fetchTableFromUrl(callback, currentTimestamp, displayName);
            } else {
                // Use the stored lookup table
                callback(lookupTable, displayName);
            }
        } else {
            // If the lookup table data or timestamp is not in local storage, fetch it from the spreadsheet
            fetchTableFromUrl(callback, currentTimestamp, displayName);
        }
    }

    function fetchTableFromUrl(callback, currentTimestamp, displayName) {
        GM.xmlHttpRequest({
            method: 'GET',
            url: spreadsheetUrl,
            responseType: 'csv',
            onload: function(response) {
                if (response.status === 200) {
                    lookupTable = parseLookupTable(response.responseText);
                    // Store the lookup table data and the current timestamp in local storage
                    localStorage.setItem('CDHlookupTable', JSON.stringify(lookupTable));
                    localStorage.setItem('CDHlookupTableTimestamp', currentTimestamp.toString());
                    callback(lookupTable, displayName);
                } else {
                    console.error('Failed to fetch lookup table data.');
                }
            },
            onerror: function(error) {
                console.error('Failed to fetch lookup table data:', error);
            }
        });
    }

    // Function to parse the CSV lookup table data into an object
    function parseLookupTable(csvData) {
        var lines = csvData.split('\n');
        var lookupTable = {};
        var headers = lines[0].split(',');

        // For some reason, adding new headers to the spreadsheet messes up the formatting. Code below fixes the formatting by removing '\r'
        for (i=0; i < headers.length; i++) {
            if (headers[i].toString().indexOf('\r') > -1) {
                headers[i] = headers[i].replace('\r','').trim();
            }
        }

        for (var i = 1; i < lines.length; i++) {
            var parts = lines[i].split(',');
            var vendor = parts[0].trim();
            for (var j = 1; j < headers.length; j++) {
                var url = parts[j].trim();
                if (vendor && url) {
                    lookupTable[vendor] = lookupTable[vendor] || {}; // Create an empty object for the vendor if it doesn't exist
                    lookupTable[vendor][headers[j]] = url; // Store the URL for the corresponding column header
                }
            }
        }
        return lookupTable;
    }

    // Function to create the button
    function createButton(lookupTable, displayName) {
        // Check to ensure the vendor exists in the lookup table
        if (lookupTable[displayName]){
            // Create a MutationObserver to monitor changes in the DOM
            var observer = new MutationObserver(function(mutationsList) {
                for (var mutation of mutationsList) {
                    if (mutation.type === 'childList') {
                        // Check if the existing div is available
                        var existingDiv = document.querySelectorAll('.info_panel__ContentContainer-sc-1p4u71o-2.hQxPlD')[document.querySelectorAll('.info_panel__ContentContainer-sc-1p4u71o-2.hQxPlD').length - 1];
                        if (existingDiv){
                            // Check if the existing div is available
                            var existingLinks = existingDiv.querySelectorAll('a');
                            var numExistingLinks = existingLinks.length;
                            var lookupLinks = Object.keys(lookupTable[displayName]);
                            var numLookupLinks = lookupLinks.length;
                            // Calculate the number of new divs needed
                            var numNewDivs = Math.ceil((numLookupLinks + numExistingLinks) / 2) - 1;

                            // Declar column, url, link vars
                            var column, url, link;

                            // Add the first link from the lookup table to the existing div
                            if (numExistingLinks < 2 && numLookupLinks > 0) {
                                column = lookupLinks[0];
                                url = lookupTable[displayName][column];
                                link = createLinkElement(column, url);
                                existingDiv.appendChild(link);
                                lookupLinks.splice(0, 1);
                                numLookupLinks--;
                            }

                            // Create new divs with links from the lookup table
                            for (var i = 0; i < numNewDivs; i++) {
                                var newDiv = document.createElement('div');
                                newDiv.setAttribute('data-test', 'panel_content_container');
                                newDiv.classList.add('info_panel__ContentContainer-sc-1p4u71o-2', 'hQxPlD');

                                // Add links to the new div
                                for (var j = i * 2; j < Math.min((i * 2) + 2, numLookupLinks); j++) {
                                    column = lookupLinks[j];
                                    url = lookupTable[displayName][column];
                                    link = createLinkElement(column, url);
                                    newDiv.appendChild(link);
                                }

                                // Redefine the existingDiv and insert the new div after the existing div
                                existingDiv = document.querySelectorAll('.info_panel__ContentContainer-sc-1p4u71o-2.hQxPlD')[document.querySelectorAll('.info_panel__ContentContainer-sc-1p4u71o-2.hQxPlD').length - 1];
                                existingDiv.parentNode.insertBefore(newDiv, existingDiv.nextSibling);
                            }

                            // Disconnect the observer to stop monitoring
                            observer.disconnect();
                            break;
                        }
                    }
                }
            })
            // Start observing changes in the document body
            observer.observe(document.body, { childList: true, subtree: true });
        }}

    // Helper function to create a link element
    function createLinkElement(column, url) {
        var link = document.createElement('a');
        link.classList.add('resource_link__Link-sc-1rmsw1q-0', 'AupFi');
        link.href = url;
        link.target = '_blank';
        link.dataset.test = 'vendor_docs_link';
        link.innerHTML = '<img src="/datacloud/static/img/vendor_docs_grey.svg" alt="Documentation image." class="resource_link__DocumentImg-sc-1rmsw1q-1 dLEIvZ"><span class="resource_link__Title-sc-1rmsw1q-2 YERGk">' + column + '</span><span class="resource_link__Description-sc-1rmsw1q-3 kQohas">Tampermonkey</span>';
        return link;
    }


    document.addEventListener('click', function(event) {
        // Check if we clicked on the "Add Connector" button to update the lookup table
        var addConnectorButton = document.querySelector('button[data-test="add-connector-button"]');
        if (event.target === addConnectorButton) {
            fetchLookupTable(createButton, null);
        }

        // Check if we clicked on a connector tile in the Connector Marketplace window
        var tileElement = event.target.closest('.connector_tile__Tile-sc-15ne86l-2.djzzeS');
        if (tileElement) {
            // Grab the connector name that was clicked on
            var displayNameElement = tileElement.querySelector('.connector_tile__DisplayName-sc-15ne86l-0.ihfopP');
            var displayName = displayNameElement.textContent;
            createButton(lookupTable, displayName);
        }
    });
})();